# places-to-know-TINDIN
#FASE 02
# Samuel da Silva Andrade - email: sasmn4@gmail.com - Wpp: (92) 99913-0594
***
**Olá, abaixo os passos para executar o programa em sua máquina:**
***
    * Passo 1: Faça o Clone para o repositório Local, o clone pode ser feito via SSH ou HTTPS, abaixo o link HTTPS:
        * https://gitlab.com/samuel.andradecpp/places-to-know-TINDIN.git
***        
    * Passo 2: Instale as dependencias:
        * Execute sua IDE (Ambiente de Desenvolvimento. ex: VSCode)
        * Selecione o diretório onde o clone foi guardado
        * Instale as dependencias dando o seguinte comando no terminal: >yarn install
        * Execute o servidor: >yarn dev
        * Após executar o servidor o mesmo estará escutando na porta 3333 em: http://localhost:27017
***
    * Passo 3: Testes de funcionalidades:
        * Para realizar os testes utilizei o Insominia, você pode baixar e instalar [aqui](https://insomnia.rest/download)
***

 * Testes Unitários Adicionados: Como testes unitáris não devem depender de aplicações externas como o Banco de Dados e a API da Unsplash criei alguns programa .js "Fakes" para simular os testes e também fiz algumas injeções de dependencia para que não fazer os programas manualmente.
